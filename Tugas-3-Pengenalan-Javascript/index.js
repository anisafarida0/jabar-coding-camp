// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var p1 = pertama.substr(0,5);
var p2 = pertama.substr(12,7);
var k1 = kedua.substr(0,8);
var k2 = kedua.substr(8,10);
var upperk2 = k2.toUpperCase();
var gabungan = p1+p2+k1+upperk2;
console.log(gabungan)

// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
var a = parseInt(kataPertama);
var b = parseInt(kataKedua);
var c = parseInt(kataKetiga);
var d = parseInt(kataKeempat);
var operasi = a+b*c+d;
console.log(operasi)

// soal 3
var kalimat = "wah javascript itu keren sekali";
var a1 = kalimat.substring(0,3);
var a2 = kalimat.substring(4,14);
var a3 = kalimat.substring(15,18);
var a4 = kalimat.substring(19, 25);
var a5 = kalimat.substring(25,32);
console.log("Kata Pertama: " + a1);
console.log("Kata Kedua: " + a2);
console.log("Kata Ketiga: " + a3);
console.log("Kata Keempat: " + a4);
console.log("Kata Kelima: " +a5)