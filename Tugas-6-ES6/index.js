// soal 1. arrow function
const luas_persegi_panjang = (panjang, lebar)=>{
	return panjang * lebar
}
console.log (luas_persegi_panjang(5, 7))

const keliling_persegi_panjang = (p, l)=> {
	return 2*(p+l)
}
console.log (keliling_persegi_panjang(2, 7))

// soal 2.
const firstName = "Farida"
const lastName = "Fikrianisa"
const fullName = `${firstName} ${lastName}`
console.log (fullName)

// soal 3. ditambahkan karakter "3" supaya variabel tidak sama dengan soal 2.
var newObject = {
	firstName3: "Muhammad",
	lastName3: "Iqbal Mubarok",
	address: "Jalan Ranamanyar",
	hobby: "playing football",
}
const{firstName3, lastName3, address, hobby} = newObject
console.log(firstName3, lastName3, address, hobby)

// soal 4.
let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [west, east]
console.log(combined)

// soal 5.
const planet = "earth"
const view = "glass"
const before = `Lorem ${view} dolor sit amet consectetur adipiscing elit, ${planet}`
console.log(before)